--
-- Populates the wikipedia_chatgpt_plugin_searches table daily,
-- filtering MediaWiki CirrusSearch request data and aggregating it by wiki.
--
-- Parameters:
--     source_table         -- Fully qualified table name of the source cirrus search request data.
--     destination_table    -- Fully qualified table name for the Wikipedia ChatGPT Plug-in search counts.
--     year                 -- Year of the partition to process.
--     month                -- Month of the partition to process.
--     day                  -- Day of the partition to process.
--
-- Usage:
--     spark3-sql -f generate_wikipedia_chatgpt_plugin_searches_daily.hql \
--         -d source_table=event.mediawiki_cirrussearch_request \
--         -d destination_table=wmf_product.wikipedia_chatgpt_plugin_searches \
--         -d year=2023 \
--         -d month=7 \
--         -d day=11
--


INSERT OVERWRITE TABLE ${destination_table}
    PARTITION(year=${year}, month=${month}, day=${day})
SELECT /*+ COALESCE(1) */
    `database` AS wiki_db,
    COUNT(1) AS search_count
FROM
    ${source_table}
WHERE
    http.request_headers['user-agent'] = 'wikipedia-chagpt-plugin bot'
    AND year = ${year}
    AND month = ${month}
    AND day = ${day}
GROUP BY `database`
;
